\documentclass[sans]{beamer}
\usetheme{metropolis}

\usepackage[linesnumbered,ruled,vlined]{algorithm2e} 
\usepackage{lmodern}
\usepackage{tikz}
\usepackage{pdfpages}
\usepackage{listings}
\usepackage{multirow}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{ulem}
\usetikzlibrary{arrows, positioning, shapes.geometric}
\usetikzlibrary{shadows.blur}
\usetikzlibrary{decorations.pathmorphing}

% hide the footline navigation
\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{navigation symbols}{}
%\setbeamertemplate{footline}{}

\tikzset{snake it/.style={decorate, decoration=snake}}

\tikzset{
    %Define standard arrow tip
    >=stealth',
}

\tikzset{
    invisible/.style={opacity=0,text opacity=0},
    visible on/.style={alt=#1{}{invisible}},
    alt/.code args={<#1>#2#3}{%
      \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}}
    },
}

\tikzset{
  setstyle/.style={#1},
  %bcg/.default={white},
  setstyle on/.style={alt=#1{}{setstyle}},
}

\tikzset{
  background fill/.style={fill=#1},
  background fill/.default={white},
  fill on/.style={alt=#1{}{background fill}},
}

\tikzset{
  background draw/.style={draw=#1},
  background draw/.default={white},
  draw on/.style={alt=#1{}{background draw}},
}

\tikzset{
  background filldraw/.style 2 args={draw=#1, fill=#2},
  background filldraw/.default={white}{white},
  filldraw on/.style={alt=#1{}{background filldraw}},
}

\tikzset{
  background shade/.style={#1},
  background shade/.default={top color=white, bottom color=white},
  shade on/.style={alt=#1{}{background shade}},
}

\tikzset{
  background shadedraw/.style 2 args={draw=#1, #2},
  background shadedraw/.default={white}{top color=white, bottom color=white},
  shadedraw on/.style={alt=#1{}{background shadedraw}},
}

\definecolor{darkgreen}{rgb}{0.0, 0.6, 0.13}

 \lstset{escapeinside={<@}{@>}, columns=fullflexible, basicstyle=\ttfamily}

 \title{\fontsize{0.905em}{1.5em}\selectfont Data-Centric Dynamic Partial Order Reduction}
 \author{\centering Andreas Pavlogiannis, \underline{Marek~Chalupa}, Krishnendu~Chatterjee,
         Nishant~Sinha, Kapil~Vaidya}
 \bigskip
 \institute {\hfill Seminar on Concurrency, 17. 5. 2018}
 \date{~\\[1cm]}

\begin{document}
  %% -------------------------------------------------------------------
  \maketitle
  %% -------------------------------------------------------------------

\section{\fontsize{0.897em}{1.5em}\selectfont Model Checking of Parallel Programs}

%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{Model Checking of Parallel Programs}

\begin{columns}
\hspace*{1.5mm}
\begin{column}{0.3\textwidth}
\vspace*{-2mm}
\begin{lstlisting}
x = 0;

thread1() {
  x = 1;
  assert(x == 1);
}

thread2() {
  x = 2;
  assert(x == 2);
}
\end{lstlisting}
\end{column}

\begin{column}{0.6\textwidth}
{\bf Visible instruction} is an instruction that modifes the global state. \\[1em]
\pause
{\bf Event} is a sequence of consecutive non-visible instructions from
one process ending with a visible instruction. \\[1em]
\pause
{\bf Conflicting Events} are two events s.t. they modify
the same memory and at~least one of the events is write.\\[1em]

\pause
{\bf Trace} is a sequence of events.
\end{column}
\end{columns}

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{Stateful model checking}

\begin{tabular}{c p{2cm} c}

\begin{lstlisting}
x = 0;

thread1() {
  x = 1;
  assert(x == 1);
}

thread2() {
  x = 2;
  assert(x == 2);
}
\end{lstlisting}
\pause
&&
\begin{tikzpicture}[overlay, xshift=1.5cm, yshift=3cm]
  \tikzset{
    state/.style={draw, minimum width=1cm},
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }


\node[state] (s1) at (0,0) { \small x = 0 };
\node[state] (s2) at(-1, -1.5) { \small x = 1 };
\node[state] (s3) at (1, -1.5) { \small x = 2 };
\node[state] (s4) at(-2, -3) { \small x = 1 };
\node[state] (s5) at (-.5, -3) { \small x = 2 };
\node[state] (s6) at (2.5, -3) { \small x = 2 };
\node[state] (s7) at (1, -3.5) { \small x = 1 };

\node[state] (s8) at (-2, -4.5) { \small x = 2 };
\node[state] (s9) at (-2, -6) { \small x = 2 };

\node[state] (s10) at(-.5, -5.3) { \small x = 2 };

\node[state] (e) at(1, -6.5) { \small \color{red}{err} };

\node[state] (s11) at(2.5, -4.5) { \small x = 1 };
\node[state] (s12) at(2.5, -6) { \small x = 1 };

\draw[->] (s1) edge node[event1]{\tiny $1$} (s2);
\draw[->] (s1) edge node[event2]{\tiny $1$}(s3);
\draw[->] (s2) edge node[event1 ]{\tiny $2$} (s4);
\draw[->] (s2) edge node[event2]{\tiny $1$}(s5);
\draw[->] (s3) edge node[event2]{\tiny $2$}(s6);
\draw[->] (s3) edge node[event1]{\tiny $1$}(s7);
\draw[->] (s4) edge node[event2]{\tiny $1$}(s8);
\draw[->] (s8) edge node[event2]{\tiny $2$}(s9);

\draw[->] (s5) edge node[event2]{\tiny $2$}(s10);
\draw[->] (s6) edge node[event1]{\tiny $1$}(s11);
\draw[->] (s11) edge node[event1]{\tiny $2$}(s12);

\draw[->] (s7) edge node[event2]{\tiny $2$}(e);
\draw[->] (s10) edge node[event1]{\tiny $2$}(e);
\draw[->] (s5) edge node[event1]{\tiny $2$}(e);


\end{tikzpicture}

\end{tabular}

\begin{tikzpicture}[overlay]
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[event1,overlay]  at (-0.3,4.38){\tiny $1$};
  \node[event1,overlay]  at (-0.3,3.78) {\tiny $2$};

  \node[event2,overlay]  at (-0.3,1.63){\tiny $1$};
  \node[event2,overlay]  at (-0.3,1.03) {\tiny $2$};
\end{tikzpicture}


\end{frame}
	%% -------------------------------------------------------------------

%%% -------------------------------------------------------------------
%\begin{frame}[fragile]
%\frametitle{Partial Order Reduction}

%\begin{columns}
%\begin{column}{0.4\textwidth}
%\centering
%Independent events\\[1em]
%\begin{tikzpicture}
%  \tikzset{
%    event1/.style={draw,circle, fill=red!20},
%    event2/.style={draw,circle, fill=blue!20},
%  }

%  \node[circle, draw] (s1) at(-1.5, 0){};
%  \node[circle, draw] (s2) at(0,  1.5) {};
%  \node[circle, draw] (s3) at(0, -1.5) {};
%  \node[circle, draw] (s4) at(1.5,0) {};

%  \draw[->] (s1) edge node[event1]{2} (s2);
%  \draw[->] (s1) edge node[event2]{2} (s3);

%  \draw[->] (s2) edge node[event2]{2} (s4);
%  \draw[->] (s3) edge node[event1]{2} (s4);

%\end{tikzpicture}
%\end{column}
%\begin{column}{0.6\textwidth}
%\pause
%{\bf Mazurkiewitcz equivalence}:\\
% Two traces are equivalent iff one can be obtained from the other by swapping
% two adjacent independent events.
%\end{column}
%\end{columns}

%\end{frame}
%%% -------------------------------------------------------------------

%%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{Partial Order Reduction}

\begin{center}
Independent events\\[1em]
\begin{tikzpicture}
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[circle, draw] (s1) at(-1.5, 0){};
  \node[circle, draw] (s2) at(0,  1.5) {};
  \node[circle, draw] (s3) at(0, -1.5) {};
  \node[circle, draw] (s4) at(1.5,0) {};

  \draw[->] (s1) edge node[event1]{2} (s2);
  \draw[->] (s1) edge node[event2]{2} (s3);

  \draw[->] (s2) edge node[event2]{2} (s4);
  \draw[->] (s3) edge node[event1]{2} (s4);

\end{tikzpicture}
\end{center}
\end{frame}
%%% -------------------------------------------------------------------


%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{Stateless model checking}

\begin{tabular}{c p{2cm} c}

\begin{lstlisting}
x = 0;

thread1() {
  x = 1;
  assert(x == 1);
}

thread2() {
  x = 2;
  assert(x == 2);
}
\end{lstlisting}
&&
\begin{tikzpicture}[overlay, xshift=1.5cm, yshift=3cm]
  \tikzset{
    state/.style={draw, circle},
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }


\node[state] (s1) at (0,0) {};
\node[state] (s2) at(-1, -1.5) { };
\node[state] (s3) at (1, -1.5) { };
\node[state] (s4) at(-2, -3) {};
\node[state] (s5) at (-.7, -3) { };
\node[state] (s6) at (2.5, -3) { };
\node[state] (s7) at (1, -3.5) {};

\node[state] (s8) at (-2, -4.5) {};
\node[state] (s9) at (-2, -6) {};

\node[state] (s10) at(-.7, -5) {};

\node[state] (s11) at(2.5, -4.5) {};
\node[state] (s12) at(2.5, -6) {};

\node[state, rectangle] (e1) at(0.3, -4) { \color{red}{err}};
\node[state, rectangle] (e2) at(-.7, -6.5) { \color{red}{err}};
\node[state, rectangle] (e3) at(1, -6) { \color{red}{err}};


\draw[->] (s1) edge node[event1]{\tiny $1$} (s2);
\draw[->] (s1) edge node[event2]{\tiny $1$}(s3);
\draw[->] (s2) edge node[event1 ]{\tiny $2$} (s4);
\draw[->] (s2) edge node[event2]{\tiny $1$}(s5);
\draw[->] (s3) edge node[event2]{\tiny $2$}(s6);
\draw[->] (s3) edge node[event1]{\tiny $1$}(s7);
\draw[->] (s4) edge node[event2]{\tiny $1$}(s8);
\draw[->] (s8) edge node[event2]{\tiny $2$}(s9);

\draw[->] (s5) edge node[event2]{\tiny $2$}(s10);
\draw[->] (s6) edge node[event1]{\tiny $1$}(s11);
\draw[->] (s11) edge node[event1]{\tiny $2$}(s12);

\draw[->] (s7) edge node[event2]{\tiny $2$}(e3);
\draw[->] (s10) edge node[event1]{\tiny $2$}(e2);
\draw[->] (s5) edge node[event1]{\tiny $2$}(e1);


\end{tikzpicture}

\end{tabular}

\begin{tikzpicture}[overlay]
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[event1,overlay]  at (-0.3,4.38){\tiny $1$};
  \node[event1,overlay]  at (-0.3,3.78) {\tiny $2$};

  \node[event2,overlay]  at (-0.3,1.63){\tiny $1$};
  \node[event2,overlay]  at (-0.3,1.03) {\tiny $2$};
\end{tikzpicture}


\end{frame}
	%% -------------------------------------------------------------------

	%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{Dynamic Partial Order Reduction}

\begin{tabular}{c p{2cm} c}

\begin{lstlisting}
x = 0;

thread1() {
  x = 1;
  assert(x == 1);
}

thread2() {
  x = 2;
  assert(x == 2);
}
\end{lstlisting}
&&
\begin{tikzpicture}[overlay,yshift=3cm,xshift=1cm]
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[circle, draw] (n0) at (1,0) {};
  \node[event1] (n1) at (0,-1) {1};
  \draw[->] (n0) edge (n1);
\pause
  \node[event1] (n2) at (-.5,-2) {2};
  \draw[->] (n1) edge (n2);
\pause
  \node[event2] (n3) at (-1,-3) {1};
  \draw[->] (n2) edge (n3);
\pause
  \only<4>{\draw[->, dashed, bend left] (n3) edge (n2);}
\pause
  \node[overlay] (set1) at (-1, -1) {$\{t2\}$};
\pause
  \node[event2] (n4) at (-1.5,-4) {2};
  \draw[->] (n3) edge (n4);
\pause
  \only<7>{\draw[->, dashed, bend left] (n4) edge (n1);}
\pause
  \node[overlay] (set0) at (-0, -0) {$\{t2\}$};
\pause
  \node[event2] (n11) at (.5,-2) {1};
  \draw[->] (n1) edge (n11);
  \node[overlay] (cross1) at (-1, -1) {\color{red}{X}};
\pause
  \node[event2] (n12) at (1,-3) {2};
  \draw[->] (n11) edge (n12);
\pause
  \only<11>{\draw[->, dashed, bend right] (n12) edge (n1);}
\pause
  \node[event1] (n13) at (1,-4) {2};
  \draw[->] (n12) edge (n13);
  \node[] (e1) at (1,-4.7) {\color{red}{err}};
\pause
  \node[overlay] (cross0) at (-0, -0) {\color{red}{X}};
  \node[event2] (n31) at (2,-1) {1};
  \draw[->] (n0) edge (n31);
\pause
  \node[event2] (n32) at (2,-2) {2};
  \draw[->] (n31) edge (n32);

  \node[event1] (n33) at (2,-3) {1};
  \draw[->] (n32) edge (n33);

  \node[event1] (n34) at (2,-4) {2};
  \draw[->] (n33) edge (n34);

  \node[overlay] (set2) at (  1.2, -1) {$\{t1\}$};
  \node[overlay] (cross2) at (1.2, -1) {\color{red}{X}};

  \node[event1] (n42) at (3,-2) {1};
  \draw[->] (n31) edge (n42);

  \node[event1] (n43) at (3,-3) {2};
  \draw[->] (n42) edge (n43);

  \node[event2] (n44) at (3,-4) {2};
  \draw[->] (n43) edge (n44);

\end{tikzpicture}


\end{tabular}

\begin{tikzpicture}[overlay]
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[event1,overlay]  at (-0.3,4.38){\tiny $1$};
  \node[event1,overlay]  at (-0.3,3.78) {\tiny $2$};

  \node[event2,overlay]  at (-0.3,1.63){\tiny $1$};
  \node[event2,overlay]  at (-0.3,1.03) {\tiny $2$};
\end{tikzpicture}


\end{frame}
	%% -------------------------------------------------------------------


\section{Data-Centric DPOR}

	%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{Motivation}

\begin{tabular}{c p{2cm} c}

\begin{lstlisting}
x = 0;

thread1() {
  x = 1;
  assert(x == 1);
}

thread2() {
  x = 2;
  assert(x == 2);
}
\end{lstlisting}
&&

\begin{tikzpicture}[overlay, yshift=-1.5cm]
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[event1] (nd11) at (0,0) {\tiny $1$};
  \node[event1] (nd12) at (1,0) {\tiny $2$};
  \node[event2] (nd13) at (2,0) {\tiny $1$};
  \node[event2] (nd14) at (3,0) {\tiny $2$};


  \node[event2] (nd21) at (0,1) {\tiny $1$};
  \node[event2] (nd22) at (1,1) {\tiny $2$};
  \node[event1] (nd23) at (2,1) {\tiny $1$};
  \node[event1] (nd24) at (3,1) {\tiny $2$};

  \node[event1] (nd31) at (0,2) {\tiny $1$};
  \node[event2] (nd32) at (1,2) {\tiny $1$};
  \node[event1] (nd33) at (2,2) {\tiny $2$};
  \node[event2] (nd34) at (3,2) {\tiny $2$};

  \node[event2] (nd41) at (0,3) {\tiny $1$};
  \node[event1] (nd42) at (1,3) {\tiny $1$};
  \node[event1] (nd43) at (2,3) {\tiny $2$};
  \node[event2] (nd44) at (3,3) {\tiny $2$};

  \draw (nd11) -- (nd12) -- (nd13) -- (nd14);
  \draw (nd21) -- (nd22) -- (nd23) -- (nd24);
  \draw (nd31) -- (nd32) -- (nd33) -- (nd34);
  \draw (nd41) -- (nd42) -- (nd43) -- (nd44);

\pause
  \node[overlay, draw, dashed, minimum width=4cm, minimum height=1.8cm] at (1.5,0.5){};

\end{tikzpicture}

\end{tabular}

\begin{tikzpicture}[overlay]
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[event1,overlay]  at (-0.3,4.38){\tiny $1$};
  \node[event1,overlay]  at (-0.3,3.78) {\tiny $2$};

  \node[event2,overlay]  at (-0.3,1.63){\tiny $1$};
  \node[event2,overlay]  at (-0.3,1.03) {\tiny $2$};
\end{tikzpicture}


\end{frame}
	%% -------------------------------------------------------------------


%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Observation Function}
For a trace $t$, be $<_t$ the total order of events in $t$.
Further, denote $W(t)$ the write events from $t$, $R(t)$ the read events
from $t$ and $Confl(e_1, e_2)$ the predicate that $e_1$ and $e_2$ are conflicting.\\[2em]

\pause

Given a trace $t$, we define the {\bf observation function} of the trace~$t$,
$O_t : R(t) \rightarrow W(t)$, such that $O_t(r) = w$ iff
\begin{itemize}
	\item[(1)]  $w <_t r$
	\item[(2)]  $\forall w' \in W(t): Confl(r, w') \implies (w' <_t w) \lor (w' >_t r)$
\end{itemize}
\vspace{1.3em}

\pause

{\bf Observation equivalence}: we say that traces $t_1$ and $t_2$ are equivalent
iff $O_{t_1}$ = $O_{t_2}$.

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{Observation Equivalence}

\begin{tabular}{c p{2cm} c}

\begin{lstlisting}
x = 0;

thread1() {
  x = 1;
  assert(x == 1);
}

thread2() {
  x = 2;
  assert(x == 2);
}
\end{lstlisting}
&&

\begin{tikzpicture}[overlay, yshift=-1.5cm]
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[event1] (nd11) at (0,0) {\tiny $1$};
  \node[event1] (nd12) at (1,0) {\tiny $2$};
  \node[event2] (nd13) at (2,0) {\tiny $1$};
  \node[event2] (nd14) at (3,0) {\tiny $2$};


  \node[event2] (nd21) at (0,1) {\tiny $1$};
  \node[event2] (nd22) at (1,1) {\tiny $2$};
  \node[event1] (nd23) at (2,1) {\tiny $1$};
  \node[event1] (nd24) at (3,1) {\tiny $2$};

  \node[event1] (nd31) at (0,2) {\tiny $1$};
  \node[event2] (nd32) at (1,2) {\tiny $1$};
  \node[event1] (nd33) at (2,2) {\tiny $2$};
  \node[event2] (nd34) at (3,2) {\tiny $2$};

  \node[event2] (nd41) at (0,3) {\tiny $1$};
  \node[event1] (nd42) at (1,3) {\tiny $1$};
  \node[event1] (nd43) at (2,3) {\tiny $2$};
  \node[event2] (nd44) at (3,3) {\tiny $2$};

  \draw (nd11) -- (nd12) -- (nd13) -- (nd14);
  \draw (nd21) -- (nd22) -- (nd23) -- (nd24);
  \draw (nd31) -- (nd32) -- (nd33) -- (nd34);
  \draw (nd41) -- (nd42) -- (nd43) -- (nd44);

  \node[overlay, draw, dashed, minimum width=4cm, minimum height=1.8cm] at (1.5,0.5){};
  \node[overlay, draw, dashed, minimum width=4cm, minimum height=.9cm] at (1.5,2){};
  \node[overlay, draw, dashed, minimum width=4cm, minimum height=.9cm] at (1.5,3){};

\end{tikzpicture}

\end{tabular}

\begin{tikzpicture}[overlay]
  \tikzset{
    event1/.style={draw,circle, fill=red!20},
    event2/.style={draw,circle, fill=blue!20},
  }

  \node[event1,overlay]  at (-0.3,4.38){\tiny $1$};
  \node[event1,overlay]  at (-0.3,3.78) {\tiny $2$};

  \node[event2,overlay]  at (-0.3,1.63){\tiny $1$};
  \node[event2,overlay]  at (-0.3,1.03) {\tiny $2$};
\end{tikzpicture}


\end{frame}
	%% -------------------------------------------------------------------



%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Annotations}
Let $W$ ($R$) be the set of write (read, resp.) events.
An~{\bf Annotation pair} $(A^+, A^-)$ is a pair of partial functions
\begin{itemize}
	\item (1) $A^+: R \rightarrow W$ called {\it possitive annotation}
	\item (2) $A^-: R \rightarrow 2^W$ called {\it negative annotation}
\end{itemize}
\vspace{1em}

A positive annotation $A^+$ is {\bf realizable} iff there exists a trace $t$,
s.t. $O_t$ = $A_+$.
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{The Algorithm}

\begin{algorithm}[H]
  \SetKwInOut{Input}{Inputs}
  \Input{A maximal trace $t$ and annotation pair $(A^+, A^-)$}

  \ForEach{read $r \in t$ in increasing order by $<_t$, $r \not \in Dom(A^+)$}{

    \ForEach{$w \in t$ s.t. $Confl(r,w)$ and $w \not \in A^-(r)$} {
			$A^+_{r,w} \gets A^+ \cup \{(r,w)\}$

			Let $t' \gets Realize(A^+_{r,w}$)

			\If {$t' \neq \bot$} {
				$t'' \gets ${maximal extension of t'}

				$A^-(r) \gets A^-(r) \cup \{w\}$

				$A_{r,w}  \gets  (A^+_{r,w}, A^-)$

				DC-DPOR(t'', $(A^+_{r,w}, A^-)$)
			}
		}
  }
\caption{DC-DPOR Algorithm}
\end{algorithm}

\end{frame}
%% ------------------------------------------------------------------
\section{Realizing Annotations}

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Realizing Annotations}
\begin{itemize}
	\item {\color{blue}{Order of events in processes}} and
	      {\color{darkgreen}{positive annotation}}  induce a partial order.

\begin{center}
\begin{tikzpicture}

\node (n1) at (-1, 1) { $w^1$ };
\node (n2) at (-1, 0) { $r^1$ };
\node (n3) at (1, 1) { $w^2$ };
\node (n4) at (1, 0) { $r^2$ };

\draw[->, color=blue] (n1) edge (n2);
\draw[->, color=blue] (n3) edge (n4);
\draw[->, color=darkgreen] (n1) edge (n4);
\only<2>{\draw[->, bend right, color=red] (n3) edge (n1);}
\end{tikzpicture}
\end{center}

$A^+ = \{(r^2, w^1)\}$
	\item[]
\pause
	\item Preserving this order is necessary but not sufficient:

\begin{itemize}
	\item[(1)]  \color{darkgreen} $w <_t r$
	\item[(2)]  \color{red}{$\forall w' \in W(t): Confl(r, w') \implies (w' <_t w) \lor (w' >_t r)$}
\end{itemize}
\end{itemize}

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{The Hardness of Realizing Annotations}
{\bf Communication graph} is an undirected graph G = (P, E) where P
is the set of processes (threads) and $E \subseteq {P \choose 2}$ is a
set of edges s.t.:\\[.5cm]
$\{p_1, p_2\} \in E$ iff $p_1$ and $p_2$ access the same shared resource.\\[2em]
\pause
\begin{itemize}
  \item The problem of realizing an annotation is NP-complete in general.
  \item The problem is polynomial with acyclic communication graph.
\end{itemize}
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Realizing Annotations in Acyclic Communication Graphs}
\begin{itemize}
	\item Reduction to 2SAT.
	\item Introduce a variable $X_{e_1,e_2}$ for each pair of events $e_1$ and $e_2$.
	\item If this variable is true, then $e_1$ should be before $e_2$
	\item Code program structure and annotation using the variables
	\item Add observation function constrains
	\item Add antisymmetry and transitivity constrains
\end{itemize}
\pause
\begin{tikzpicture}[overlay]
\node[] (n1) at (5.4,1) {};
\node[align=left] (n2) at (6.5,-0.3) { \color{red} Can be coded to 2SAT\\
 																			 \color{red} because of the acyclicity};
\draw[->, thick, color=red] (n2) edge (n1);
\end{tikzpicture}

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Realizing Annotations in Cyclic Communication Graphs}
Make the communication graph acyclic:
\begin{itemize}
	\item Fix order of events between some processes P1 and P2 (try all possibilities of the order).
	\item Remove the edge $\{P1, P2\}$ from the communication graph.
	\item Repeat until you get an acyclic graph.
\end{itemize}

Then reduce to 2SAT.

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Conclusions}
\begin{itemize}
	\item Model checking algorithm for parallel programs based on
	      more coarser equivalence on traces.
	\item May explore exponentially less traces.
  \item Spends polynomail time exploring each equivalence class
	      (for cyclic communication graphs the equivalence is less coarser, though).
\end{itemize}
\pause
\vspace*{1cm}
\hfill Thank you!
\end{frame}
%% -------------------------------------------------------------------

\end{document}
