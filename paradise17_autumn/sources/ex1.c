#include <stdlib.h>

extern int input(void);

int main(void) {
	int a = input();
	int *p;

	p = NULL;
	if (a < 1) {
		p = &a;
		*p = 1;
	}

	*p = 0;

	return 0;
}
