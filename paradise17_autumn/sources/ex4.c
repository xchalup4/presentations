#include <stdlib.h>

struct A {
	void *(*f1)(int *);
	void *(*f2)(int *);
};

void *f1(int *a) {
	return a;
}

void *f2(int *a) {
	return NULL;
}

int main(void) {
	int a;
	struct A cb = {.f1 = f1, .f2 = f2};

	cb.f2 = f1;
	cb.f1 = f2;

	int *p = cb.f1(&a);
	*p = 3;

	return 0;
}
