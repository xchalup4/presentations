#!/bin/sh

LLVM_PATH=$HOME/src/symbiotic-dbg/llvm-3.9.1/build/bin
DG_PATH=$HOME/src/symbiotic-dbg/dg/tools

export PATH=$LLVM_PATH:$DG_PATH:$PATH

clang -emit-llvm -S $1 -o code.ll
shift
ps-show $@ code.ll
